<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->bigInteger('customer_id');
            $table->bigInteger('product_type_id');
            $table->bigInteger('price');
            $table->bigInteger('date_finish');
            $table->bigInteger('winner_id');
            $table->tinyInteger('mailing_purchases_mode');
            $table->tinyInteger('public_mode');
            $table->boolean('provider_visibility_for_providers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}

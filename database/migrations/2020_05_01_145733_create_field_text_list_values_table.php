<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFieldTextListValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_text_list_values', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('entity_type_id');
            $table->bigInteger('entity_id');
            $table->bigInteger('field_id');
            $table->bigInteger('variant_id');
            $table->bigInteger('serial_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_text_list_values');
    }
}

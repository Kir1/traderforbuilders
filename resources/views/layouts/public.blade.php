@extends('layouts.main')

@section('layout')
    <div class="container">
        @yield('content')
    </div>
@endsection

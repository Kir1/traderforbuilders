export const simpleField = {
    props: ['field', 'index'],
    inject: ["changeFieldValue"],
    computed: {
        value: {
            get: function () {
                return this.field.values[this.index].value;
            },
            set: function (v) {
                return this.field.values[this.index].value;
            }
        }
    }
}


export const form = {
    props: {
        entityId: {
            type: Number,
            default: -1
        }
    },
    data: () => ({
        form: null,
        requestStatus: 0,
        originalForm: null
    }),
    methods: {
        getForm() {
            return this.form;
        },
        getField(group_name, field_name) {
            return this.form.fieldsGroups[group_name][field_name];
        },
        getFieldValues(group_name, field_name) {
            return this.getField(group_name, field_name).values;
        },
        changeFieldValue(group_name, field_name, value, index) {
            this.getFieldValues(group_name, field_name)[index].value = value;
        },
        addValue(group_name, field_name) {
            this.getFieldValues(group_name, field_name).push({...this.getField(group_name, field_name).template});
        },
        removeValue(group_name, field_name, index) {
            this.getFieldValues(group_name, field_name)[index].delete = true;
        },
        getFieldType(field_id) {
            try {
                for (let groupName in this.form.fieldsGroups)
                    for (let fieldName in this.form.fieldsGroups[groupName])
                        if (this.form.fieldsGroups[groupName][fieldName].field_id == field_id)
                            throw this.form.fieldsGroups[groupName][fieldName].field_type_id;
            } catch (field_type_id) {
                return field_type_id;
            }
            return null;
        },
        groupWithNonEmptyFields(group_name) {
            for (let fieldName in this.form.fieldsGroups[group_name]) {
                if (this.form.fieldsGroups[group_name][fieldName].values.length > 0) return true;
            }
            return false;
        },
    },
    provide: function () {
        return {
            getForm: this.getForm,
            changeFieldValue: this.changeFieldValue,
            addValue: this.addValue,
            removeValue: this.removeValue,
            groupWithNonEmptyFields: this.groupWithNonEmptyFields
        }
    }
}


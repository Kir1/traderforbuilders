export const user = {
    state: {
        user: null
    },
    mutations: {
        AUTH: (state, data) => {
            state.user = data;
        }
    },
    actions: {},
    getters: {}
}

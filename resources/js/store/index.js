import {user} from './modules/user/index';
import {login} from './modules/login/index';
import {register} from './modules/register/index';
import {profile} from './modules/profile/index';

export const store = {
    modules: {
        user,
        login,
        register,
        profile
    }
}


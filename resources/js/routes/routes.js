import PageError404 from "../components/pages/PageError404";
import PageContainer from "../components/pages/PageContainer";
import PageFieldsGroups from "../components/pages/fields/PageFieldsGroups";
import PageFields from "../components/pages/fields/PageFields";
import PageProviders from "../components/pages/provider/PageProviders";
import PageProviderCreate from "../components/pages/provider/PageProviderCreate";
import PageProviderEdit from "../components/pages/provider/PageProviderEdit";
import PageProvider from "../components/pages/provider/PageProvider";

export const routes = [
    {path: '/spa-app/fields-groups', name: 'fields-groups', component: PageFieldsGroups},
    {path: '/spa-app/fields', name: 'fields', component: PageFields},
    {path: '/spa-app/providers',  name: 'providers', component: PageProviders},
    {
        path: '/spa-app/providers',
        component: PageContainer,
        children: [
            {
                path: 'create',
                name: 'create-provider',
                component: PageProviderCreate
            },
            {
                path: ':id',
                name: 'provider',
                component: PageProvider
            },
            {
                path: ':id/edit',
                name: 'edit-provider',
                component: PageProviderEdit
            },
        ]
    },
    {path: '*', component: PageError404, name: '404'}
]

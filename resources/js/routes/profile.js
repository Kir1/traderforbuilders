import PageProfile from "../components/pages/profile/PageProfile";
import PageProfiles from "../components/pages/profile/PageProfiles";

export const profile = {
    path: '/profile',
    redirect: {name: '404'},
    children: [
        {
            path: 'list',
            name: 'profile-list-page',
            component: PageProfiles
        },
        {
            path: ':id',
            name: 'profile-page',
            component: PageProfile
        }
    ]
}

export function axiosSimpleRequest($this, url, resultFunction, data = null, updateState = true) {
    $this.requestStatus = 0;
    axios.post(url, data)
        .then(result => {
            $this.requestStatus = 1;
            if (updateState) resultFunction($this,result);
        })
        .catch(errors => {
            $this.requestStatus = 1;
            if (errors.response.data.errors) {
                let str = '';
                for (let field in errors.response.data.errors) {
                    errors.response.data.errors[field].forEach((error) => {
                        str += error + '\n';
                    });
                }
                alert(str);
            } else alert('Ошибка сервера! Перезагрузите страницу.');
        })
}

function axiosRequest($this, url, data = null, actionName = '', requestStatusProperty = '') {
    return axios.post(url, data)
        .then(result => {
            $this.$store.commit(actionName, result.data);
            $this[requestStatusProperty] = 1;
        })
        .catch(errors => {
            $this[requestStatusProperty] = 2;
            throw errors;
        })
}

export function axiosRequestForComponent($this, url, data = null, actionName = '', requestStatusProperty = '', errorsProperty = '') {
    axiosRequest($this, url, data, actionName, requestStatusProperty)
        .catch(errors => {
            if (errors.response.data.errors instanceof Object)
                $this[errorsProperty] = {...errors.response.data.errors};
            else
                $this[errorsProperty] = {error: ['Упс! Ошибка на сервере.']};
        })
}

export function axiosRequestForState() {

}

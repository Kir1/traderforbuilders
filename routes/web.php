<?php

use Illuminate\Support\Facades\Route;

Route::post('auth', 'Main@auth');

Auth::routes();

Route::prefix('spa-app')->group(function () {
    Route::prefix('fields-groups')->group(function () {
        Route::post('/', 'Fields\FieldGroupController@index');
        Route::post('create', 'Fields\FieldGroupController@create');
        Route::post('{id}/update', 'Fields\FieldGroupController@update');
    });

    Route::prefix('fields')->group(function () {
        Route::post('/', 'Fields\FieldController@index');
        Route::post('create', 'Fields\FieldController@create');
        Route::post('{id}/update', 'Fields\FieldController@update');
        Route::post('{id}/delete', 'Fields\FieldController@delete');
    });

    Route::prefix('customers')->group(function () {
        Route::post('/', 'CustomerController@index');
        Route::post('fields', 'CustomerController@fields');
        Route::post('{id}/entity', 'CustomerController@entity');
        Route::post('create', 'CustomerController@create');
        Route::post('{id}/update', 'CustomerController@update');
        Route::post('{id}/delete', 'CustomerController@delete');
    });

    Route::prefix('providers')->group(function () {
        Route::post('/', 'ProviderController@index');
        Route::post('fields', 'ProviderController@fields');
        Route::post('{id}/entity', 'ProviderController@entity');
        Route::post('create', 'ProviderController@create');
        Route::post('{id}/update', 'ProviderController@update');
        Route::post('{id}/delete', 'ProviderController@delete');
    });

    Route::prefix('purchases')->group(function () {
        Route::post('/', 'PurchaseController@index');
        Route::post('fields', 'PurchaseController@fields');
        Route::post('{id}/entity', 'PurchaseController@entity');
        Route::post('create', 'PurchaseController@create');
        Route::post('{id}/update', 'PurchaseController@update');
        Route::post('{id}/delete', 'PurchaseController@delete');
    });

    Route::get('/', function () {
        return view('layouts.non-public');
    });
    Route::get('/{any?}', function ($any) {
        return view('layouts.non-public');
    })->where('any', '.*');
});

//Route::middleware(['anonimus'])->group(function () {
//    Route::post('login', 'Auth/LoginController@login');
//    Route::post('register', 'Auth/RegisterController@lregister');
//});
//
//Route::middleware(['authorized'])->group(function () {
//    Route::post('logout', 'Auth/LoginController@logout');
//});

Route::get('/', 'PublicController@index');
Route::get('/{page?}', 'PublicController@index');

<?php

namespace App\Services\CMS;

use App\Models\Fields\Field;
use App\Models\Fields\FieldNumRange;
use App\Services\ErrorService;
use Illuminate\Support\Facades\Validator;

class CMSService
{
    const FIELD_TYPES = [
        ['type' => 'bool', 'name' => 'Логический', 'id' => 1, 'model' => 'App\Models\Fields\FieldBoolValue'],
//        ['type' => 'num', 'name' => 'Число', 'id' => 2],
        ['type' => 'text', 'name' => 'Текст', 'id' => 3, 'model' => 'App\Models\Fields\FieldTextValue'],
//        ['type' => 'html_text', 'name' => 'Большой текст с html', 'id' => 4],
        //['type' => 'num_range', 'name' => 'Диапозон чисел', 'id' => 5],
//        ['type' => 'list_text', 'name' => 'Список(слов)', 'id' => 6],
//        ['type' => 'file', 'name' => 'Файл', 'id' => 7],
        //8 => 'date',
        //9 => 'date_range'
        // 6 => 'list_date',
    ];

    //сущности, у которых есть кастомные поля из таблиц
    const ENTITY_TYPES = [
        [
            'model' => 'App\Models\User',
            'name' => 'Заказчик',
            'id' => 1
        ],
        [
            'model' => 'App\Models\User',
            'name' => 'Поставщик',
            'id' => 2
        ],
        [
            'model' => 'App\Models\Purchase',
            'name' => 'Закупка',
            'id' => 3
        ]
    ];

    const USER_ROLES = [
        [
            'name' => 'Администратор',
            'id' => 1
        ],
        [
            'name' => 'Модератор',
            'id' => 2
        ],
        [
            'name' => 'Заказчик',
            'id' => 3
        ],
        [
            'name' => 'Поставщик',
            'id' => 4
        ],
    ];

    public static function getFieldTypeProperty($field_type_id, $propertyName = 'model')
    {
        foreach (CMSService::FIELD_TYPES as $field_type) if ($field_type_id == $field_type['id']) return $field_type[$propertyName];
        return null;
    }

    public static function getEntityTypeProperty($entity_type_id, $propertyName = 'name')
    {
        foreach (CMSService::ENTITY_TYPES as $entity) if ($entity_type_id == $entity['id']) return $entity[$propertyName];
        return null;
    }

    /**
     * Получает поля для сущности, а если передана id сущности, то и значения полей
     *
     * @param integer $entity_type_id тип сущности из ENTITY_TYPES
     * @param integer $entity_id = 0 айди сущности(для получения значений полей)
     *
     * @return []
     *
     * @Rest\Post("/login")
     */
    public static function getFeildsAndValues($entity_type_id, $entity_id = 0)
    {
        $form = null;

        $form = [
            'entity_type_id' => $entity_type_id,
            'entity_type_name' => CMSService::getEntityTypeProperty($entity_type_id, 'name'),
            'fieldsGroups' => ['Общие поля' => []]
        ];

        $fields = Field::where('entity_type_id', $entity_type_id)->with('feild_group')->get();

        if ($fields->isNotEmpty()) {
            foreach ($fields as $field) {
                if ($field->feild_group) $form['fieldsGroups'][$field->feild_group->name] = [];
            }

            $group_name = '';
            $valueFieldValue = null;

            foreach ($fields as $field) {
                $group_name = isset($field->feild_group) ? $field->feild_group->name : 'Общие поля';

                $field_name = $field->name . " (Машинное имя: " . $field->machine_name . ")";

                $arrayField = [
                    'field_id' => $field->id,
                    'field_type_id' => $field->field_type_id,
                    'group_name' => $group_name,
                    'field_name' => $field_name,
                    'multiple' => $field->multiple
                ];

                //дополнительные поля для типа
                if ($field->field_type_id === 5) {
                    $range = FieldNumRange::where('field_id', $field->id)->first();
                    if ($range) $arrayField['range'] = [
                        'from' => $range->from_value,
                        'to' => $range->to_value
                    ];
                }

                //значения по дефолту
                $template = null;
                switch ($field->field_type_id) {
                    case 1: //логич
                        $template = ['value' => true, 'delete' => false];
                        break;
                    case 3: //текст
                        $template = ['value' => '', 'delete' => false];
                        break;
                    case 5: //диапозон чисел
                        $template = ['value' => $range->from_value, 'delete' => false];
                        break;
                }

                $arrayField['template'] = $template;
                $arrayField['values'] = [$template];

                //значения от сущности при редактировании
                if ($entity_id) {
                    $arrayField['values'] = [];

                    $form['entity_id'] = $entity_id;
                    $entityModel = CMSService::getEntityTypeProperty($entity_type_id, 'model');
                    $entity = $entityModel::find($entity_id);
                    if ($entity) $form['entity_name'] = $entity->name; //у всех сущностей должно быть это поле!!!
                    else return ErrorService::returnError('Сущность не найдена');

                    $modelField = CMSService::getFieldTypeProperty($field->field_type_id);

                    $queryBuilder = $modelField::where([
                        ['entity_type_id', '=', $field->entity_type_id],
                        ['entity_id', '=', $entity_id],
                        ['field_id', '=', $field->id]
                    ]);

                    $values = $queryBuilder->get();

                    if ($values->isNotEmpty()) foreach ($values as $value) {
                        $valueFieldValue = $value->value;
                        if ($field->field_type_id === 1) $valueFieldValue = (boolean)$valueFieldValue;
                        $arrayField['values'] [] = ['id' => $value->id, 'delete' => false, 'value' => $valueFieldValue];
                    }
                }

                $form['fieldsGroups'][$group_name][$field_name] = $arrayField;
            }
        }

        return ['form' => $form];
    }

    public static function form($entity_id, $updateMode = false)
    {
        $request = request();
        $data = [];
        $rules = [];
        $nameFieldValidate = '';
        foreach ($request->form['fieldsGroups'] as $fieldsGroup) {
            foreach ($fieldsGroup as $nameField => $field) {
                $data[$nameField] = $field['values'];
                $nameFieldValidate = $nameField . ".*.value";
                switch ($field['field_type_id']) {
                    case 1:
                        $rules[$nameFieldValidate] = 'boolean';
                        break;
                    case 3:
                        $rules[$nameFieldValidate] = 'string';
                        break;
                }
                //if ($updateMode == false) $rules[$nameFieldValidate] .= '|required';
            }
        }

        Validator::make($data, $rules)->validate();

        $modelValue = null;
        foreach ($request->form['fieldsGroups'] as $fieldsGroup) {
            foreach ($fieldsGroup as $field) {
                foreach ($field['values'] as $index => $fieldValue) {
                    ///
                    $modelValue = CMSService::getFieldTypeProperty($field['field_type_id']);

                    //////delete
                    if ($fieldValue['delete']) {
                        if (isset($fieldValue['id'])) $modelValue::destroy($fieldValue['id']);
                        continue;
                    }

                    ///////save(update or create)
                    if (isset($fieldValue['id'])) $modelValue = $modelValue::find($fieldValue['id']);
                    else $modelValue = new $modelValue();

                    switch ($field['field_type_id']) {
                        case 1:
                        case 3:
                            if (isset($fieldValue['id']) == false) {
                                $modelValue->entity_type_id = $request->form['entity_type_id'];
                                $modelValue->entity_id = $entity_id;
                                $modelValue->field_id = $field['field_id'];
                            }
                            $modelValue->value = $fieldValue['value'];
                            break;
                    }

                    $modelValue->save();
                }
            }
        }
    }
}


<?php

namespace App\Services;

use App\Models\User as UserModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserService
{
    public static function createUserWithProfile(
        $email, $password, $name, $surname, $avatarFile = null, $birthday = '', $sex = ProfileService::SEX_UNDEFINED
    )
    {
        $user = UserService::createUser($email, $password, $name, $surname, $avatarFile);
        $profile = ProfileService::createProfile($user['id'], $birthday, $sex);
        return ['user' => $user, 'profile' => $profile];
    }

    public static function createUser($email, $password, $name, $surname, $avatarFile = null)
    {
        $avatar_micro = '';
        $avatar_mini = '';
        $avatar_middle = '';
        $avatar_original = '';
        if ($avatarFile) {
            //
        }
        $result = [
            'email' => $email,
            'password' => Hash::make($password),
            'name' => $name,
            'surname' => $surname,
            'avatar_micro' => $avatar_micro,
            'avatar_mini' => $avatar_mini,
            'avatar_middle' => $avatar_middle,
            'avatar_original' => $avatar_original
        ];
        $result['id'] = UserModel::create($result)->id;
        return $result;
    }

    public static function getUserMinifined($userId = 0)
    {
        if ($userId === 0) {
            $userId = Auth::id();
            if ($userId == null) return null;
        }
        $user = UserModel::find($userId);
        if ($user) $user = UserService::formingUserMinifined($user->toArray());
        return $user;
    }

    public static function formingUserMinifined(array $user)
    {
        return ['id' => $user['id'], 'name' => $user['name'], 'avatar' => $user['avatar_micro']];
    }
}

<?php

namespace App\Http\Controllers;

use App\Services\CMS\CMSService;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Str;

class ProviderController extends Controller
{
    public function index(Request $request)
    {
        return User::orderBy('id', 'desc')
            ->where('role_id', 2)
            ->select('id', 'name')
            ->paginate($request->itemsPerPage ?? 5);
    }

    public function fields(Request $request)
    {
        return CMSService::getFeildsAndValues(2);
    }

    public function entity(Request $request, $id)
    {
        return CMSService::getFeildsAndValues(2, $id);
    }

    public function create(Request $request)
    {
        $user = User::create([
            'email' => Str::random(4),
            'password' => Str::random(5),
            'name' => Str::random(8),
            'area_id' => 1,
            'tin' => random_int(1, 999),
            'role_id' => 2
        ]);

        CMSService::form($user->id);
        return '';
    }

    public function update(Request $request, $id)
    {
        //if (Auth::id() != $id) return ErrorService::returnError403();
        CMSService::form($id, true);
        return '';
    }

    public function delete(Request $request, $id)
    {
        User::destroy($id);
        return '';
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Fields\Field;
use App\Models\Fields\FieldGroup;
use App\Models\Fields\FieldNumRange;
use App\Service\UserService;
use App\Http\Controllers\Controller;
use App\Services\CMS\CMSService;
use Illuminate\Http\Request;
use App\User;

class CustomerController extends Controller
{
    function recordsWithPaginate()
    {
        $fields = Field::orderBy('id', 'desc')
            ->with('num_range')
            ->paginate($request->itemsPerPage ?? 5);
        return [
            'fields' => $fields,
            'fields_groups' => FieldGroup::all(),
            'fields_types' => CMSService::FIELD_TYPES,
            'entities_types' => CMSService::ENTITY_TYPES,
        ];
    }

    function saveSubFields($entity, $request, $createMode = true)
    {
        if ($entity && $entity->field_type_id === 5) {
            $max = $request->to_value - 1;
            $min = $request->from_value + 1;
            $request->validate([
                'from_value' => "required|numeric|max:$max",
                'to_value' => "required|numeric|min:$min",
            ]);

            if ($createMode) {
                $range = new FieldNumRange();
                $range->field_id = $entity->id;
            } else $range = FieldNumRange::where('field_id', $entity->id)->first();

            if ($range) {
                $range->from_value = $request->from_value;
                $range->to_value = $request->to_value;
                $range->save();
            }
        };
    }

    public function index(Request $request)
    {
        return $this->recordsWithPaginate();
    }

    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:fields',
            'machine_name' => [
                'required',
                'unique:fields',
                'regex:/^([a-z0-9]+(?:-[a-z0-9]+)*)$/'
            ],
            'field_type_id' => 'required|numeric',
            'entity_type_id' => 'required|numeric',
            'group_id' => 'required|numeric',
            'multiple' => 'boolean'
        ]);

        $entity = new Field();
        $entity->name = $request->name;
        $entity->machine_name = $request->machine_name;
        $entity->field_type_id = $request->field_type_id;
        $entity->entity_type_id = $request->entity_type_id;
        $entity->group_id = $request->group_id;
        if ($request->multiple) $entity->multiple = $request->multiple;
        $entity->save();

        $this->saveSubFields($entity, $request);

        return $this->recordsWithPaginate();
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'group_id' => 'numeric',
        ]);

        $entity = Field::find($id);
        if ($request->name != $entity->name) $request->validate([
            'name' => 'unique:fields',
        ]);

        $entity->name = $request->name;
        if ($entity->group_id) $entity->group_id = $request->group_id;
        $entity->save();

        $this->saveSubFields($entity, $request, false);

        return '';
    }

    public function delete(Request $request, $id)
    {
        Field::destroy($id);
        return '';
    }
}

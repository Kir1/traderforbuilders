<?php

namespace App\Http\Controllers\Fields;

use App\Models\Fields\FieldGroup;
use App\Service\UserService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class FieldGroupController extends Controller
{
    function recordsWithPaginate()
    {
        return FieldGroup::orderBy('id', 'desc')->paginate($request->itemsPerPage ?? 5);
    }

    public function index(Request $request)
    {
        return $this->recordsWithPaginate();
    }

    public function create(Request $request)
    {
        $request->validate([
            'machine_name' => [
                'required',
                'unique:field_groups',
                'regex:/^([a-z0-9]+(?:-[a-z0-9]+)*)$/'
            ],
            'name' => 'required|unique:field_groups'
        ]);

        $entity = new FieldGroup();
        $entity->name = $request->name;
        $entity->machine_name = $request->machine_name;
        $entity->save();

        return $this->recordsWithPaginate();
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:field_groups'
        ]);

        $entity = FieldGroup::find($id);
        $entity->name = $request->name;
        $entity->save();

        return '';
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    public $timestamps = false;

    //
    protected $fillable = [
        'name', 'user_id'
    ];
}

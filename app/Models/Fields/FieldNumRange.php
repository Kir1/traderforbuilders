<?php

namespace App\Models\Fields;

use Illuminate\Database\Eloquent\Model;

class FieldNumRange extends Model
{
    public $timestamps = false;

    //
    protected $fillable = [
        'field_id', 'from_value', 'to_value'
    ];
}

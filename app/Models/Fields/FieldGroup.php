<?php

namespace App\Models\Fields;

use Illuminate\Database\Eloquent\Model;

class FieldGroup extends Model
{
    public $timestamps = false;

    //
    protected $fillable = [
        'name', 'machine_name'
    ];
}

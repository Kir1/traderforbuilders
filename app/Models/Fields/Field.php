<?php

namespace App\Models\Fields;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    public $timestamps = false;

    //group_id по-умолчанию 0, что значит, что группая общая
    //
    protected $fillable = [
        'name', 'machine_name', 'field_type_id', 'entity_type_id', 'group_id', 'multiple', 'serial_number'
    ];

    public function feild_group()
    {
        return $this->hasOne('App\Models\Fields\FieldGroup', 'id' , 'group_id');
    }

    public function num_range()
    {
        return $this->hasOne('App\Models\Fields\FieldNumRange', 'field_id' , 'id');
    }
}

<?php

namespace App\Models\Fields;

use Illuminate\Database\Eloquent\Model;

class FieldTextListVariants extends Model
{
    public $timestamps = false;

    //
    protected $fillable = [
        'field_id', 'value'
    ];
}

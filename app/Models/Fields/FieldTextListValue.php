<?php

namespace App\Models\Fields;

use Illuminate\Database\Eloquent\Model;

class FieldTextListValue extends Model
{
    public $timestamps = false;

    //
    protected $fillable = [
        'entity_type_id', 'entity_id', 'field_id', 'value', 'serial_number'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseRequest extends Model
{
    //
    protected $fillable = [
        'provider_id', 'purchase_id', 'status', 'files'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    //
    protected $fillable = [
        'customer_id', 'product_type_id', 'price', 'date_finish', 'winner_id', 'mailing_purchases_mode',
        'public_mode', 'provider_visibility_for_providers'
    ];
}

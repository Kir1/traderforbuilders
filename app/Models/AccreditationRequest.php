<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccreditationRequest extends Model
{
    //
    protected $fillable = [
        'provider_id', 'customer_id', 'status', 'files'
    ];
}

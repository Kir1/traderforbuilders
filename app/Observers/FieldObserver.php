<?php

namespace App\Observers;

use App\Field;
use App\Models\Fields\FieldBoolValue;
use App\Models\Fields\FieldTextValue;

class FieldObserver
{
    /**
     * Handle the field "created" event.
     *
     * @param \App\Field $field
     * @return void
     */
    public function created(Field $field)
    {
        //
    }

    /**
     * Handle the field "updated" event.
     *
     * @param \App\Field $field
     * @return void
     */
    public function updated(Field $field)
    {
        //
    }

    /**
     * Handle the field "deleted" event.
     *
     * @param \App\Field $field
     * @return void
     */
    public function deleted(Field $field)
    {
        FieldBoolValue::where('field_id', $field->id)->delete();
        FieldTextValue::where('field_id', $field->id)->delete();
    }

    /**
     * Handle the field "restored" event.
     *
     * @param \App\Field $field
     * @return void
     */
    public function restored(Field $field)
    {
        //
    }

    /**
     * Handle the field "force deleted" event.
     *
     * @param \App\Field $field
     * @return void
     */
    public function forceDeleted(Field $field)
    {
        //
    }
}
